import telegram
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.dates as mdates
import io
import pandas as pd
import pandahouse
from read_db.CH import Getch
import os

sns.set()

def test_report(chat=None):
    my_chat_id = 484756871
    bot = telegram.Bot(token='1378305191:AAEGPzkWRRSEY6Rx3oAIbGyH__Ix7CfVVwM')
    
    data_dau = Getch(
    '''
    select count(distinct user_id) as DAU,
    toDate(time) as day
    from simulator.feed_actions
    where toDate(time) = today() - 1
    group by day
    order by day desc
    '''
    ).df
    
    data_view = Getch(
    '''
    select action,
    count(user_id) as number_of_view
    from simulator.feed_actions
    where toDate(time) = today() - 1 and
    action == 'view'
    group by action
    '''
    ).df
    
    data_like = Getch(
    '''
    select action,
    count(user_id) as number_of_like
    from simulator.feed_actions
    where toDate(time) = today() - 1 and
    action == 'like'
    group by action
    '''
    ).df
    
    data_ctr = Getch(
    '''
    select *
    from 
        (
        select toDate(time) as day,
        countIf(user_id, action='like')/countIf(user_id, action='view') as CTR
        from simulator.feed_actions
        group by toDate(time)
        ) as t1
    where day = today() - 1
    '''
    ).df
    
    msg = f'Аналитическая сводка за {data_ctr.day.to_string(index=False)}:\n\
    DAU = {round(int(data_dau.DAU.to_string(index=False))/1000)}K\n\
    view = {round(int(data_view.number_of_view.to_string(index=False))/1000)}K\n\
    like = {round(int(data_like.number_of_like.to_string(index=False))/1000)}K\n\
    CTR = {round(float(data_ctr.CTR.to_string(index=False)), 3):.0%}'

    bot.sendMessage(chat_id=my_chat_id, text=msg)
    
    
    data_dau_7 = Getch(
    '''
    select toStartOfDay(time) as day,
    count(distinct user_id) as DAU
    from simulator.feed_actions
    where toStartOfDay(time) > today() - 8 and toStartOfDay(time) < today()
    group by day
    order by day desc
    '''
    ).df
    
    data_view_7 = Getch(
    '''
    select toStartOfDay(time) as day,
    count(user_id) as number_of_view
    from simulator.feed_actions
    where toStartOfDay(time) > today() - 8 and toStartOfDay(time) < today() and
    action == 'view'
    group by toStartOfDay(time)
    order by day desc
    '''
    ).df
    
    data_like_7 = Getch(
    '''
    select toStartOfDay(time) as day,
    count(user_id) as number_of_like
    from simulator.feed_actions
    where toStartOfDay(time) > today() - 8 and toStartOfDay(time) < today() and
    action == 'like'
    group by toStartOfDay(time)
    order by day desc
    '''
    ).df
    
    data_ctr_7 = Getch(
    '''
    select *
    from 
        (
        select toStartOfDay(time) as day,
        countIf(user_id, action='like')/countIf(user_id, action='view') as CTR
        from simulator.feed_actions
        group by toStartOfDay(time)
        ) as t1
    where day > today() - 8 and  day < today()
    order by day desc
    '''
    ).df
    
    metric_df = data_view_7.merge(data_like_7, on='day').merge(data_dau_7, on='day').merge(data_ctr_7, on='day')
    
    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(18, 16))
    sns.lineplot(data=metric_df, x='day', y='number_of_view', ax=ax[0][0])
    sns.lineplot(data=metric_df, x='day', y='number_of_like', ax=ax[0][1])
    sns.lineplot(data=metric_df, x='day', y='DAU', ax=ax[1][0])
    sns.lineplot(data=metric_df, x='day', y='CTR', ax=ax[1][1])

    myFmt = mdates.DateFormatter('%d-%b')
    ax[0][0].xaxis.set_major_formatter(myFmt)
    ax[0][1].xaxis.set_major_formatter(myFmt)
    ax[1][0].xaxis.set_major_formatter(myFmt)
    ax[1][1].xaxis.set_major_formatter(myFmt)

    ax[0][0].grid(visible=True)
    ax[0][1].grid(visible=True)
    ax[1][0].grid(visible=True)
    ax[1][1].grid(visible=True)

    ax[0][0].set_title('Количество просмотров\n')
    ax[0][1].set_title('Количество лайков\n')
    ax[1][0].set_title('Число уникальных пользователей в день\n')
    ax[1][1].set_title('Показатель кликабельности\n');

    # зададим файловый объект
    plot_object = io.BytesIO()
    # сохраним в него наш график
    plt.savefig(plot_object)
    # зададим имя нашему файловому объекту
    plot_object.name = 'metric_plot.png'
    # перенесем курсор из конца файлового объекта в начало, чтобы прочитать весь файл
    plot_object.seek(0)
    # закроем файл
    plt.close()
    # отправим изображение
    bot.sendPhoto(chat_id=my_chat_id, photo=plot_object)
    

try:
    test_report()
except Exception as e:
    print(e)
   